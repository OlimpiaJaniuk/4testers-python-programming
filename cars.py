def get_country_of_car_brand(car_brand):
    if car_brand in ("toyota", "mazda", "suzuki", "subaru"):
        return "Japan"
    elif car_brand in ("bmw", "mercedes", "audi", "volkswagen"):
        return "Germany"
    elif car_brand in ("renault", "peugeot", "citroen"):
        return "France"
    else:
        return "Unknown"

def get_gas_usage_for_distance(distance_in_kilometers, average_gas_usage_per_100_km):
    if distance_in_kilometers < 0 or average_gas_usage_per_100_km < 0:
        return 0
    return distance_in_kilometers / 100 * average_gas_usage_per_100_km



if __name__ == '__main__':
    print(get_country_of_car_brand("bmw"))
    print(get_country_of_car_brand("suzuki"))
    print(get_country_of_car_brand("lamborgini"))





