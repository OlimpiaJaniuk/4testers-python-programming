# 1
def get_list_number(list_with_numbers):
    return sum(list_with_numbers)

temperatures_in_january = [-4, 1.0, -7, 2]
temperatures_in_february = [-13, -9, -3, 3]

print(f"January: {get_list_number(temperatures_in_january)}")
print(f"February: {get_list_number(temperatures_in_february)}")
def get_average_of_two_numbers(number_1, number_2):
    return (number_1 + number_2) /  2

print(get_average_of_two_numbers(2, 3))

# 2
import uuid
def get_random_data_of_email (user_email):
    password = uuid.uuid4()
    return {
    "email": user_email,
    "password": f"{password}"
}
print(get_random_data_of_email("ol@example.pl"))
print(get_random_data_of_email("ol@example2.pl"))
print(get_random_data_of_email("ol@example3.pl"))
print(get_random_data_of_email("ol@example4.pl"))

# Exercise 3
