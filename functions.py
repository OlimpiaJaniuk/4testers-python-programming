name = "Ada"
def print_word_length_description(name):
    if len(name) > 5:
        print(f'The name {name} is longer than characters')
    else:
        print(f'The name {name} is 5 characters or shorter')

if __name__ == '__main__':
    long_name = "Adrianna"
    short_name = "Adam"
    print_word_length_description(long_name)
    print_word_length_description(short_name)

#2
def check_if_normal_conditions(celsius, pressure):
   if celsius == 0 and pressure == 1013:
       return True
   else:
       return False

if __name__ == '__main__':
    print(check_if_normal_conditions(0, 1013))
    print(check_if_normal_conditions(1, 1014))
    print(check_if_normal_conditions(0, 1014))
    print(check_if_normal_conditions(1, 1013))

#3
def is_it_4testers_day(day_of_week):
    if day_of_week == "monday" or day_of_week == "wednesday" or day_of_week == "friday":
        return True
    else:
        return False
if __name__ == '__main__':
    print(is_it_4testers_day("monday"))
    print(is_it_4testers_day("thursday"))

#4
def is_it_japanese_car_brand(car_brand):
    if car_brand in ["Toyota", "Suzuki", "Mazda"]:
        return True
    else:
        return False

if __name__ == '__main__':

    print(is_it_japanese_car_brand("BMW"))
    print(is_it_japanese_car_brand("Kia"))
    print(is_it_japanese_car_brand("Toyota"))


# 3
    def is_it_4testers_day(day_of_week):
        if day_of_week == "monday" or day_of_week == "wednesday" or day_of_week == "friday":
            return True
        else:
            return False


    if __name__ == '__main__':
        print(is_it_4testers_day("monday"))
        print(is_it_4testers_day("thursday"))


# 4
    def is_it_japanese_car_brand(car_brand):
        if car_brand in ["Toyota", "Suzuki", "Mazda"]:
            return True
        else:
            return False


    if __name__ == '__main__':
        print(is_it_japanese_car_brand("BMW"))
        print(is_it_japanese_car_brand("Kia"))
        print(is_it_japanese_car_brand("Toyota"))

#5
def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >= 75:
        return 4
    elif number_of_points >= 50:
        return 3
    else:
        return 2

if __name__ == '__main__':
    print(get_grade_for_exam_points(91))
    print(get_grade_for_exam_points(90))
    print(get_grade_for_exam_points(89))
    print(get_grade_for_exam_points(76))
    print(get_grade_for_exam_points(75))
    print(get_grade_for_exam_points(74))
    print(get_grade_for_exam_points(50))
    print(get_grade_for_exam_points(49))
