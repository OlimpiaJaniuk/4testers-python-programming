# 1
movies = ["Paragraf 22", "Catch me if you can", "Incepcja", "To nie jest świat dla starych ludzi", "Bękarty wojny"]
first_movie = movies[0]

print(f"first movie is {first_movie}")

last_movie = movies[-1]
print(f"last_movie is {last_movie}")

movies.append("Dune")

print(f"The length of movie list is {len(movies)}")

print(f'The length of word supercalifragilistic is {len("supercalifragilistic")}')

movies.insert(0, "Star wars")
movies.remove("Incepcja")
print(movies)

movies[-1] = "Dune (1980)"
print(movies)

# 2

emails = ["a@example.com", "b@example.com"]
first_emails = emails[0]
last_emails = emails[-1]
print(f"the length of email list is {len(emails)}")
print(f"first email is {first_emails}")
print(f"last email is {last_emails}")
emails.append("cde@example.com")
print(emails)



emails = ["a@example.com", "b@example.com"]
print(len(emails))
print(emails[0])
print(emails[-1])
emails.append("cde@example.com")
print(emails)
