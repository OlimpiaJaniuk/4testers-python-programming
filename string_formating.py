# Exercise 1
name = "Michał"
city = "Toruń"

print(f"Witaj {name}! Miło Cię widzieć w naszym mieście {city}!")

name = "Beata"
city = "Gdynia"

print(f"Witaj {name}! Miło Cię widzieć w naszym mieście {city}!")

def print_hello_message(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście {city}!")

print_hello_message("Michał", "Toruń")
print_hello_message("Beata", "Gdynia")

# Exercise 2
def get_email(name, surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"

print(get_email("Janusz", "Nowak"))
print(get_email("Barbara", "Kowalska"))