# Exercise 1
friend = {
    "name": "Monika",
    "age": 43,
    "hobbies": ["traveling", "reading"]
}

print(friend["age"])
friend["city"] = "Wrocław"
friend['age'] = 39
print(friend)
friend["job"] = "doctor"
print(friend)
del friend["job"]
print(friend)

print(friend["hobbies"][-1])
friend["hobbies"].append("climbing")
print(friend)

def get_list_number(list_with_numbers):
    return sum(list_with_numbers)

print(f"January: {get_list_number([-4, 1.0, -7, 2])}")
print(f"February: {get_list_number([-13, -9, -3, 3])}")