#Exercise 1 example 2
def get_average(numbers_list):
    return sum(numbers_list)  /len(numbers_list)

january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]

print(get_average(january))
print(get_average(february))

def get_average_of_two_num(no1, no2):
    return (no1 + no2) / 2


print(get_average_of_two_num(2, 3))

january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]


# Exercise 2: Random password generator
import uuid
def get_random_login_data(user_email):
    password = uuid.uuid4()
    return {
        "email": user_email,
        "password": f"{password}"
    }

print(get_random_login_data("ol@sdfghj"))
print(get_random_login_data("ol1@sdfghj"))
print(get_random_login_data("ol2@sdfghj"))
print(get_random_login_data("ol3@sdfghj"))
print(get_random_login_data("ol4@sdfghj"))

#Exercise 3

def gamer_description(gamer_dictionary):
    print(f"The player {gamer_dictionary['nick']} is of type {gamer_dictionary['type']} and has {gamer_dictionary['exp_points']} EXP")

description = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp_points": 3000
}

print_gamer_description(player_1)
print_gamer_description(player_2)