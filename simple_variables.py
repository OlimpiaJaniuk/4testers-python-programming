my_name = "Olimpia"
my_age = 34
my_email = "olimpia.janiuk@gmail.com"

print(my_name)
print(my_age)
print(my_email)

my_friend_name = "Monika"
my_friend_age = 44
my_friend_animals_amount = 2
does_my_friend_has_driving_license = True
how_many_years_we_known_each_other = 29

print(my_friend_name)
print(my_friend_age)
print(my_friend_animals_amount)
print(does_my_friend_has_driving_license)
print(how_many_years_we_known_each_other)
print(my_friend_name, my_friend_age, my_friend_animals_amount, does_my_friend_has_driving_license,
      how_many_years_we_known_each_other)
