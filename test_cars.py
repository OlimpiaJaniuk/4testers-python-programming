from cars import get_country_of_car_brand, get_gas_usage_for_distance

def test_get_country_for_a_japanese_car():
    assert get_country_of_car_brand("toyota") == "Japan"

def test_get_country_for_a_german_car():
    assert get_country_of_car_brand("mercedes") == "Germany"

def test_get_country_for_a_german_car():
    assert get_country_of_car_brand("volkswagen") == "Germany"

def test_get_country_for_a_france_car():
    assert get_country_of_car_brand("peugeot") == "France"

def test_get_country_for_a_italian_car():
    assert get_country_of_car_brand("fiat") == "Unknown"

#Test for a function calculating gas usage
def test_gas_usage_for_zero_distance_driven():
    assert get_gas_usage_for_distance(0, 8.5) == 0

def test_gas_usage_for_100_distance_driven():
    assert get_gas_usage_for_distance(100, 8.5) == 8.5

def test_gas_usage_for_negative_distance_driven():
    assert get_gas_usage_for_distance(-50, 8.5) == 0

def test_gas_usage_for_zero_gas_usage_driven():
    assert get_gas_usage_for_distance(50, 0) == 0

def test_gas_usage_for_negative_gas_usage_driven():
    assert get_gas_usage_for_distance(50, -10) == 0

