import uuid
def print_10_random_uuids():
    for i in range(10):
        print(str(uuid.uuid4()))

if __name__ == '__main__':
    print_10_random_uuids()

#2
def print_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))

if __name__ == '__main__':
    print_random_uuids(15)

#3
def print_numbers_from_20_to_30():
    for number in range(20, 31):
        print(number)

if __name__ == '__main__':
    print_numbers_from_20_to_30()


#4
def print_numbers_from_20_to_30_multiplied_by_4():
    for number in range(20, 31):
        print(number*4)

if __name__ == '__main__':
    print_numbers_from_20_to_30_multiplied_by_4()


#5
def print_number_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number+1):
        if number % 7 == 0:
            print(number)

if __name__ == '__main__':
    print_number_divisible_by_7(1, 30)

#6
def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        temp_fahrenheit = temp * 9 / 5 + 32
        print(f"Celsius: {temp}, Fahrenheit: {temp_fahrenheit}")

if __name__ == '__main__':
    temp_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temperatures_in_both_scales(temp_celsius)

#7
def get_temperatures_higher_then_20_degrees(list_of_temps_in_celsius):
    filtered_temperatures = []
    for temp in list_of_temps_in_celsius:
        if temp > 20:
            filtered_temperatures.append(temp)
    return filtered_temperatures

if __name__ == '__main__':
    print(get_temperatures_higher_then_20_degrees(temp_celsius))




