def print_temperature_description(temperature_in_celsius):
    print(f"temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("it's getting hot!")
    else:
        print("it's quite OK")


if __name__ == '__main__':
    temperature_in_celsius = 14
    print_temperature_description(temperature_in_celsius)


def check_temp_press(temparature_in_celsius, pressure_in_hp):
    if temparature_in_celsius == 0 and pressure_in_hp == 1013:
        return True
    else:
        return False


print(check_temp_press(0, 1013))
print(check_temp_press(0, 1014))
print(check_temp_press(1, 1014))
