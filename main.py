my_name = "Olimpia"
my_age = 44
my_email = "olimpia.janiuk@gmail.com"

print(my_name)
print(my_age)
print(my_email)

# The description of my friend
friend_name = "Monika"
friend_age = 44
friend_amount_animals = 5
driving_licences = "True"
friendship_time_in_years = 27.5

print(friend_name, friend_age, friend_amount_animals, driving_licences, friendship_time_in_years, sep=";")
