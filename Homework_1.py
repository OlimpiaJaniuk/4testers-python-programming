# Exercise 1
def get_number_square(num):
    return num ** 2

zero_square = get_number_square(0)
print(zero_square)

sixteen_square = get_number_square(16)
print(sixteen_square)

floating_number_square = get_number_square(2.55)
print(floating_number_square)


# Exercise 2
def get_cuboid_volume(a, b, c):
    return a * b * c

print(get_cuboid_volume(3, 5, 7))


# Exercise 3
def convert_from_celsius_to_fahrenheit(temperature_degree):
    return (temperature_degree * 1.8) + 32

print(convert_from_celsius_to_fahrenheit(20))
